
    <?php


    
    //autoloader
    require '../vendor/autoload.php';
    

    //Validator
    include '../app/validation/validate.php';
   
   

    //Message/Notice board header
    include '../app/templates/header.php';


    //Message/Notice board form
    include '../app/templates/form.php';

    //Message/Notice board table
    include "../app/templates/table.php";