<?php
namespace Helper;

class CheckHour
{
	public function checkWorkingHours()
	{
		$current = (int)date("H", time());
		return $current >= 8 && $current <= 17;
	}
}

?>