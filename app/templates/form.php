<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Message/Notice board</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" 
            crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
        <form  method="post"enctype='multipart/form-data' >
            <div class="form-group">
                <label for="exampleInputName">Name</label>
                <input type="text" class="form-control" id="inputName" placeholder="Enter Name" name="name">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="text" class="form-control" id="inputEmail1"  placeholder="Enter email" name="email">
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Message</label>
                <textarea class="form-control" id="inputMessage" rows="4" placeholder="Enter your message" name="message"></textarea>
                <small  class="form-text text-muted">Please enter your message.Message limit is 500 characters.</small>
            </div>
            <div class="form-group">
                <label for="exampleFormControlFile1">Add image</label>
                <input type="file" class="form-control-file" id="addImage" name="image">
                <small  class="form-text text-muted">Your image must be .jpg or .png file,and 192x192 resolution.</small>
            </div>
            <button type="submit"  class="btn btn-primary">Submit</button>
        </form>
        <br><br>
        <hr>
    </body>
</html>