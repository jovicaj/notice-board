<?php
namespace Database;
use mysqli;

class Fetch
{
	public function selectData()
	{
		$connection = new \database\Connection();
		$connection->connect();
		$query = "SELECT * FROM board WHERE time ORDER BY 1 DESC;";
		$result = mysqli_query($connection->conn, $query);
		$this->result = $result;
	}


	public function fetchData () 
	{
		while($row = mysqli_fetch_array($this->result)) {
	
			$insert = new \validation\UserInput();
			$this->no = $row['no'];
		 	$this->name = $row['name'];
		 	$this->email = $row['email'];
		 	$this->message = $row['message'];
		 	$this->image = $row['image'];
		
			 echo '
    		<tr>
      		<th scope="row">'.$this->no.'</th>
      		<td>' .$this->name .'</td>
      		<td>'.$this->email.'</td>
      		<td>'.$this->message.'</td>
      		<td><img src="data:image/png;base64,'.base64_encode($this->image) .'" height="192" width="192" class="img-thumnail" /></td>
			</tr>';

		}
	}
		
}






