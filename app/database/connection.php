<?php

namespace Database;
use mysqli;


 
class Connection
{   
	private $host;
	private $user;
	private $password;
	private $db;
	
	public function __construct()
	{
		$db = include 'config.php';
		$this->host = $db['host'];
		$this->user = $db['user'];
		$this->password = $db['password'];
		$this->db = $db['db'];
	}


	public function connect()
	{ 
		$conn = new mysqli($this->host, $this->user, $this->password, $this->db);
		$this->conn = $conn;
		return $this->conn;
	}

	public function close()
	{
		mysqli::close($this->conn);
	}
}

?>
