<?php
namespace Validation;

class UserInput
{

    public $name;
    public $email;
    public $message;
    public $image;

    function __construct()
    {
        $this->name = $_POST['name'];
        $this->email = $_POST['email'];
        $this->message = $_POST['message'];
        $this->image = addslashes(file_get_contents($_FILES["image"]["tmp_name"]));
   
    }
    

}
